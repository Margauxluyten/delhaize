package com.vdab.services;

import com.vdab.domain.Store;
import com.vdab.repositories.StoreRepository;

import java.util.List;

public class StoreService {
StoreRepository storeRepository = new StoreRepository();

    public List<Store> getStores() {
        return storeRepository.getStores();
    }
}
