package com.vdab.services;

import com.vdab.domain.Customer;
import com.vdab.repositories.CustomerRepository;

import java.util.List;

public class CustomerService {

    CustomerRepository customerRepository =  new CustomerRepository();

    public List<Customer> showAllCustomers() {
        return customerRepository.showAllCustomers();
    }
}
