package com.vdab;

import com.vdab.domain.*;
import com.vdab.services.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class DelhaizeMain {

    Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
    AddingToDatabaseService addingToDatabaseService = new AddingToDatabaseService();
    ProductService productService= new ProductService();
    DeletingFromDBService deletingFromDBService = new DeletingFromDBService();
    StoreService storeService = new StoreService();
    CustomerService customerService =new CustomerService();


    public static void main(String[] args) {

        DelhaizeMain delhaizeMain = new DelhaizeMain();
        delhaizeMain.showInitialOptions();
    }

    private void showInitialOptions() {

        System.out.println("please select an option of the below : " +
                "\n0 .Quit" +
                "\n1. Fill up Database" +
                "\n2. Delete from Database" +
                "\n3. Make new order "+
                "\n4. Show list of products "
                );

        int choice = scanner.nextInt();
        switch (choice) {
            case 0:
                System.exit(0);
                break;
            case 1:
                chooseClassToAdd();
                break;
            case 2:
                chooseDeleteClass();
                break;
            case 3:
                makeAnOrder();
                break;
            case 4:
                showListOfProducts();
                break;


        }
    }




    private void chooseClassToAdd() {
        System.out.println("Please select class object to add to Database \n" +
                "0. Go back \n" +
                "1. Add Products \n" +
                "2.  \n" +
                "3.  \n" +
                "4.  \n" +
                "5.  \n"
        );
        String choice = scanner.next();
        switch (choice) {
            case "0":
                showInitialOptions();
                break;
            case "1":
                addAProduct();
                break;
            case "2":
                //addACategory();
                break;


        }
    }


    private void addAProduct() {
        System.out.println("Please select a category\n"+
                        "\n1. Food "+
                        "\n2. Cleaning" +
                        "\n3. Personal care  "+
                        "\n4. Drinks "+
                        "\n5. Pet Food "
                );
       Category category = addingToDatabaseService.getCategoryById(scanner.nextInt());
        System.out.println("what is the name of the product?");
        String name = scanner.next();
        System.out.println("Type in the description of the product?");
        String description = scanner.nextLine();
        scanner.nextLine();
        System.out.println("what is the retail price of the product ? ");
        Double retail = scanner.nextDouble();
        System.out.println("What is the price of the product ?");
        Double price = scanner.nextDouble();
        System.out.println("How many items adding to stock ?");
        int stock = scanner.nextInt();
        System.out.println("Type in the id of the store you want to sign the product too");
       List <Store> storeList = storeService.getStores();
       for(Store s : storeList){
           System.out.println(s.getId()+" " +s.getStoreName());
       }
       int choice1 = scanner.nextInt();




        addingToDatabaseService.addProductToDB(Product.builder()
                .productName(name)
                .wholeSale(price)
                .description(description)
                .category(category)
                .retailPrice(retail)
                .quantity(stock)
                .store(Store.builder().id(choice1).build())
                .build());
        System.out.println("Do you want to add another product ?"+
                "\n 1. yes "+
                "\n 2. no ");
        String choice = scanner.nextLine();
        if(choice.equals("1")){
            addAProduct();
        }else{
            showInitialOptions();
        }

    }

    private void chooseDeleteClass() {
        System.out.println("Please select class to delete from database "+
                "\n0. Go back "+
                "\n1. Delete product "
                );
        int choice = scanner.nextInt();
        switch (choice){
            case 0:
                showInitialOptions();
                break;
            case 1:
                deleteProductById();
                break;
        }

    }

    private void deleteProductById() {
        List<Product>productList=productService.listOfProducts();
        for(Product p : productList){
            System.out.println(p.getId() + " "+ p.getProductName());
        }
        System.out.println("Enter the id of the product you want to remove");
        DeletingFromDBService.deleteProductById(scanner.nextInt());
        showInitialOptions();
    }

    private void makeAnOrder() {
        List<Customer>customerList= customerService.showAllCustomers();
        for(Customer c : customerList){
            System.out.println(c.getId()+" "+ c.getName());
        }
        System.out.println("Give us the id of the customer you wish to make an order for ");
        int customerId = scanner.nextInt();
        List<Product>orderList = new ArrayList<>();
        boolean wannaEnd= false;
        while(wannaEnd == false ){
            List<Product>productList=productService.listOfProducts();
            for(Product p : productList){
                System.out.println(p.getId() + " "+ p.getProductName()+" "+p.getQuantity());
            }
            System.out.println("choose a product by id of the product you wish to buy? ");
            int productId = scanner.nextInt();
            System.out.println("Choose the quantity of the product?");
            int quantity= scanner.nextInt();

        }

    }

    private void showListOfProducts() {
        System.out.println("Do you wish a complete list of products or you want a stocklist of a specific store ?"+
                "\n1. Complete list "+
                "\n2. Specific store list ");
        int choice = scanner.nextInt();
        if(choice == 1){
            List<Product>productList=productService.listOfProducts();
            for(Product p : productList) {
                System.out.println(p.getId() + " " + p.getProductName());
            }
            }else{
            System.out.println("give the id of the store you want the product list from ");
            List <Store> storeList = storeService.getStores();
            for(Store s : storeList){
                System.out.println(s.getId()+" " +s.getStoreName());
                System.out.println("-----------------------------");
            }
            int storeId = scanner.nextInt();
            List<Product>productList=productService.listOfSpecificProducts(storeId);
            for(Product p : productList){
                System.out.println(p.toString());
            }
        }
    }
}
